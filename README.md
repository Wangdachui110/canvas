# canvas-drawing-board

这是我制作的画图板，实践过程中遇到的问题和相关知识已经记录在我的博客中。

![示例图片](https://gitee.com/Wangdachui110/canvas/blob/master/src/example.png)

## 预览

[源码地址](https://gitee.com/Wangdachui110/canvas)
[预览地址](https://wangdachui110.gitee.io/canvas/dist/index.html)

## 博客

[《制作 Canvas 画图板》](https://blog.csdn.net/weixin_44809439/article/details/110299272)

## 更新

- 优化移动端显示
- 新增橡皮功能

## 运行

1. `git clone https://gitee.com/Wangdachui110/canvas.git`
2. 进入 `canvas/dist`文件
3. 用浏览器打开 index.html
